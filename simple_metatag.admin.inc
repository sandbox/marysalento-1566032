<?php

/**
 *
 * @return string 
 */
function simple_metatag_not_implement() {
  $output = '<div class="simple metatag not implemented">';
  $output .= '<p><strong>Functionality implemented soon. </strong></p>';
  $output .= '</div>';
  return $output;
}

?>
